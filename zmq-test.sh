#!/bin/bash

if (($# != 1))
then
	echo ""
	echo "This script starts zeromq ping-pong"
	echo "The argument is: ./zeromq-test maxnum"
	echo ""
	
	exit 1
	
fi

max_num=$1;

if ((max_num < 1))
then
	echo ""
	echo "Argument should be >= 1"
	echo ""
	
	exit 1
	
fi

echo "Testing is started..."
echo

./zmq-server $max_num &
./zmq-client $max_num &
wait; wait;

echo
echo "Testing is finished!"
