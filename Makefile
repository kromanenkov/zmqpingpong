CC=gcc
CFLAGS=-o2 -lzmq

all:
	$(CC) $(CFLAGS)	zmq-client.c -o zmq-client
	$(CC) $(CFLAGS)	zmq-server.c -o zmq-server

clean:
	rm zmq-client zmq-server
