#include <zmq.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#define END_STR "fin"


// Receive 0MQ string from socket and convert into C string
// Caller must free returned string. Returns NULL if the context
// is being terminated.
static char *
s_recv (void *socket) {
    char buffer [256];
    int size = zmq_recv (socket, buffer, 255, 0);
    if (size == -1)
        return NULL;
    if (size > 255)
        size = 255;
    buffer [size] = 0;
    return strdup (buffer);
}

// Convert C string to 0MQ string and send to socket
static int
s_send (void *socket, char *string) {
    int size = zmq_send (socket, string, strlen (string), 0);
    return size;
}


int main(int argc, char **argv) {

	int maxnum, curnum = 0;
	maxnum = atoi(argv[1]);
	int endflag = 0;
	
	//Prepare context and socket
	void *context = zmq_ctx_new ();
	void *responder = zmq_socket (context, ZMQ_REP);
	zmq_bind (responder, "tcp://*:5555");
	
	//Buffer for recieved messages
	char *recv_p;
	printf("Server is started\n");
	
	//Time structures
	time_t curtime = time (NULL);
	struct tm *loctime;
	char *formatted;
	
	while (!endflag) {
		//Get request from client
		recv_p = s_recv(responder);
	
		if (!strcmp(recv_p, END_STR)) //Get finish message
			endflag = 2;	
		if (!endflag && ((curnum = atoi(recv_p)) >= maxnum)) { //Send finish message to 2nd process
			s_send(responder, END_STR);
			endflag = 1;
		}
		
		if (endflag != 2) { //Don't get finish message yet
			formatted = (char *) malloc(10);
			curtime = time(NULL);
			loctime = localtime(&curtime);
			strftime(formatted, 10, "%H:%M:%S", loctime);
			printf("Server TimeStamp - %s; Recieved num = %d\n", formatted, curnum);
			fflush(stdout);
			free(formatted);
		}
		
		sprintf(recv_p, "%d", curnum + 1);
		sleep(1); //Simulate latency
		//Send reply to client
		s_send(responder, recv_p);
		free(recv_p);
	}
	
	printf("Server is shutting down\n");
	zmq_close(responder);
	zmq_ctx_destroy(context);	
}
