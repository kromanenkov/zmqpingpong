#include <zmq.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#define END_STR "fin"
#define BUF_SIZE 256


// Receive 0MQ string from socket and convert into C string
// Caller must free returned string. Returns NULL if the context
// is being terminated.
static char *
s_recv (void *socket) {
    char buffer [256];
    int size = zmq_recv (socket, buffer, 255, 0);
    if (size == -1)
        return NULL;
    if (size > 255)
        size = 255;
    buffer [size] = 0;
    return strdup (buffer);
}

// Convert C string to 0MQ string and send to socket
static int
s_send (void *socket, char *string) {
    int size = zmq_send (socket, string, strlen (string), 0);
    return size;
}


int main(int argc, char **argv) {

	int maxnum, curnum = 0;
	maxnum = atoi(argv[1]);
	int endflag = 0;

	//Prepare context and socket
	void *context = zmq_ctx_new();
	void *requester = zmq_socket(context, ZMQ_REQ);
	zmq_connect(requester, "tcp://localhost:5555");
	
	//Buffers for recieved and sent messages
	char sendbuf[BUF_SIZE];
	char *recv_p;
	printf("Client is started\n");
	
	//Time structures
	time_t curtime;
	struct tm *loctime;
	char *formatted;
    
	while (!endflag) {
		sprintf(sendbuf, "%d", curnum + 1);
		sleep(1); //Simulate latency
		//Send request to server
		s_send(requester, sendbuf);
		//Get reply from server
		recv_p = s_recv(requester);
		
		if (!strcmp(recv_p, END_STR)) //Get finish message
			endflag = 2;		
		if (!endflag && ((curnum = atoi(recv_p)) >= maxnum)) { //Send finish message to 2nd process 
			s_send(requester, END_STR);
			endflag = 1;
		}
		
		if (endflag != 2) { //Don't get finish message yet
			formatted = (char *) malloc(10);
			curtime = time(NULL);
			loctime = localtime(&curtime);
			strftime(formatted, 10, "%H:%M:%S", loctime);
			printf("Client TimeStamp - %s; Recieved num = %d\n", formatted, curnum);
			fflush(stdout);
			free(formatted);
		}
		free(recv_p);			
	}
	
	printf("Client is shutting down\n");
	zmq_close(requester);
	zmq_ctx_destroy(context);
}

